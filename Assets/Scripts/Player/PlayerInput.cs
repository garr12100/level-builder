﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{

    public Movement movement;
    public Jump jump;

    void Start()
    {
        if(movement == null)
            movement = GetComponent<Movement>();
        if(jump == null)
            jump = GetComponent<Jump>();
    }

    private void Update()
    {
        if (movement != null)
        {
            movement.MoveX = Input.GetAxisRaw(UtilityStrings.Input.Horizontal);
            movement.MoveY = Input.GetAxisRaw(UtilityStrings.Input.Vertical);
            movement.running = Input.GetButton(UtilityStrings.Input.Run);

        }

        if (jump != null)
        {
            if(Input.GetButtonDown(UtilityStrings.Input.Jump))
                jump.JumpThisFrame = true;
            if (Input.GetButton(UtilityStrings.Input.Jump))
                jump.AttemptingToJump = true;
            else
                jump.AttemptingToJump = false;
        }
    }
}
