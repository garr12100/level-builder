﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler_Destroy : DeathHandler
{
    public override void Die()
    {
        base.Die();
        Destroy(gameObject);
    }
}
