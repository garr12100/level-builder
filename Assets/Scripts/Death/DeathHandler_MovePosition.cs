﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeathHandler_MovePosition : DeathHandler
{
    public float respawnDelay = 3f;
    public Transform respawnPosition;

    public UnityEvent OnRespawn;

    public override void Die()
    {
        base.Die();
        ObjectLoadingManager.Instance.StartCoroutine(Respawn());
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnDelay);
        if (respawnPosition != null)
        {
            gameObject.transform.position = respawnPosition.position;
            var health = GetComponent<Health>();
            health.FullRestore();
            OnRespawn.Invoke();
        }
    }
}
