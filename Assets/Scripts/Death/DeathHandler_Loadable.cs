﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler_Loadable : DeathHandler
{
    private LoadableObject loadableObject;

    private void Awake()
    {
        loadableObject = GetComponent<LoadableObject>();
    }

    public override void Die()
    {
        base.Die();
        if (loadableObject == null)
            return;
        loadableObject.Unload();
        loadableObject.isLoadable = false;
    }
}
