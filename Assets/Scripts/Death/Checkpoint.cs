﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Transform respawnPoint;
    public bool deactivateOnUse = true;
    public List<Checkpoint> checkpointGroup = new List<Checkpoint>();

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag(UtilityStrings.Tags.Player))
        {
            if(other.TryGetComponent<DeathHandler_MovePosition>(out var deathHandler))
            {
                deathHandler.respawnPosition = respawnPoint;
                if (deactivateOnUse)
                    this.gameObject.SetActive(false);
                foreach (var checkpoint in checkpointGroup)
                    checkpoint.gameObject.SetActive(true);
            }
        }
    }
}
