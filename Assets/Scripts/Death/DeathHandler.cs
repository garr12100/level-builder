﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

abstract public class DeathHandler : MonoBehaviour
{
    public UnityEvent OnDie;

    public virtual void Die()
    {
        OnDie.Invoke();
    }
}
