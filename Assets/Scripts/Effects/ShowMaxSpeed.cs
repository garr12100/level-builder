﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMaxSpeed : MonoBehaviour
{
    public Movement movement;
    public GameObject maxSpeedIndicator;

    private void Start()
    {
        if (movement == null)
            movement = GetComponent<Movement>();
    }

    private void FixedUpdate()
    {
        if (maxSpeedIndicator == null)
            return;
        maxSpeedIndicator.SetActive(movement != null && movement.AtMaxSpeed);
    }
}
