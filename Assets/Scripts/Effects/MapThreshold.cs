﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MapThreshold : MonoBehaviour
{
    public float threshold = -1;

    public ObjectLimits objectLimits;
    public ObjectLimit checkAgainst = ObjectLimit.TopPoint;
    public ComparisonOperator comparison = ComparisonOperator.Less;
    public UnityEvent OnBeyondThreshold;

    private void Start()
    {
        if (objectLimits == null)
            objectLimits = GetComponent<ObjectLimits>();
    }

    // Update is called once per frame
    void Update()
    {
        float pos = transform.position.y;
        if (objectLimits != null)
        {
            switch (checkAgainst)
            {
                case ObjectLimit.TopPoint:
                    pos = objectLimits.topPoint.position.y;
                    break;
                case ObjectLimit.RightPoint:
                    pos = objectLimits.rightPoint.position.x;
                    break;
                case ObjectLimit.BottomPoint:
                    pos = objectLimits.bottomPoint.position.y;
                    break;
                case ObjectLimit.LeftPoint:
                    pos = objectLimits.leftPoint.position.x;
                    break;
            }
            
        }
        bool beyondThreshold = false;
        switch (comparison)
        {
            case ComparisonOperator.Greater:
                beyondThreshold = pos > threshold;
                break;
            case ComparisonOperator.Equal:
                beyondThreshold = pos == threshold;
                break;
            case ComparisonOperator.Less:
                beyondThreshold = pos < threshold;
                break;
            case ComparisonOperator.GreatEqual:
                beyondThreshold = pos >= threshold;
                break;
            case ComparisonOperator.LessEqual:
                beyondThreshold = pos <= threshold;
                break;
        }
        if (beyondThreshold)
            OnBeyondThreshold.Invoke();
    }
}
