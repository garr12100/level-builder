﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingColor : MonoBehaviour
{
    public Vector2 size = Vector2.one;
    public Vector2Int pieces = new Vector2Int(10, 10);
    public Vector2 explosionForceRange = new Vector2(.005f, .05f);
    public Color color;
    public GameObject particlePrefab;
    public List<(GameObject obj, SpriteRenderer sprite, Rigidbody2D rbody)> cachedObjects;

    public void Explode(Transform t)
    {
        if(cachedObjects == null)
        {
            cachedObjects = new List<(GameObject, SpriteRenderer, Rigidbody2D)>();
            for (int i = 0; i < pieces.x * pieces.y; i++)
            {
                var obj = Instantiate(particlePrefab);
                var sprite = obj.GetComponentInChildren<SpriteRenderer>();
                var rbody = obj.GetComponentInChildren<Rigidbody2D>();
                cachedObjects.Add((obj, sprite, rbody));
            }
        }
        Vector3 bottomLeft = new Vector3(t.position.x - size.x / 2, t.position.y, t.position.z);
        Vector2 center = new Vector3(t.position.x, t.position.y + size.y / 2);
        var xSpace = size.x / pieces.x;
        var ySpace = size.y / pieces.y;
        var force = Random.Range(explosionForceRange.x, explosionForceRange.y);
        var radius = Mathf.Max(size.x, size.y) * 1.5f;
        for (int i = 0; i < pieces.x; i++)
        {
            for(int j = 0; j < pieces.y; j++)
            {
                var particle = cachedObjects[pieces.x * i + j];
                particle.obj.SetActive(true);
                particle.obj.transform.localScale = new Vector3(xSpace, ySpace);
                particle.obj.transform.position = new Vector3(bottomLeft.x + i * xSpace, bottomLeft.y + j * ySpace, bottomLeft.z);
                particle.obj.transform.rotation = Quaternion.identity;
                particle.sprite.color = color;
                particle.rbody.velocity = Vector2.zero;
                particle.rbody.angularVelocity = 0;
                particle.rbody.AddExplosionForce(force, center + (Vector2.one * Random.Range(.001f, .01f)), radius);
            }
        }
    }
}
