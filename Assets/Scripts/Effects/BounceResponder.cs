﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceResponder : MonoBehaviour
{
    public Rigidbody2D rbody;
    public Jump jump;

    public void Bounce(float bounceForce, float jumpBounceForce)
    {
        if (rbody == null)
            return;
        if (jump == null)
            rbody.velocity = new Vector2(rbody.velocity.x, bounceForce);
        else
        {
            float f = (jump != null && jump.AttemptingToJump) ? jumpBounceForce : bounceForce;
            jump.PerformJump(f);
        }
    }
}
