﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour
{
    public float bounceForce;
    public float jumpBounceForce;
    public ObjectLimits objectLimits;
    public LimitsComparer limitsComparer;
    public bool bounceOnContact = true;
    public bool checkLimits = true;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (bounceOnContact)
            AttemptBounce(other.GetComponent<BounceResponder>());

    }

    public void AttemptBounce(BounceResponder bounceResponder)
    {
        if (bounceResponder == null || bounceResponder.rbody == null)
            return;

        ObjectLimits otherLimits = bounceResponder.GetComponent<ObjectLimits>();
        if (checkLimits && (otherLimits == null || objectLimits == null))
            return;
        if (!checkLimits || limitsComparer.Compare(objectLimits, otherLimits))
        {
            bounceResponder.Bounce(bounceForce, jumpBounceForce);
        }
    }
}
