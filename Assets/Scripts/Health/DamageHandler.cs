﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageHandler : MonoBehaviour
{
    public event System.Action<GameObject, float, Vector2> OnDamage;
    public Health health;

    public void TakeDamage(GameObject damager, float amount, Vector2 direction = new Vector2())
    {
        if (OnDamage != null)
            OnDamage(damager, amount, direction);
        if (health != null)
            health.TakeDamage(amount, direction);

    }

    public void TakeDamage_NoData(float amount)
    {
        if (health != null)
            health.TakeDamage(amount);
    }

}
