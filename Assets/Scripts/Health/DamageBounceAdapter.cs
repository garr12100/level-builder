﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageBounceAdapter : MonoBehaviour
{
    public DamageHandler damageHandler;
    public Bounce bounce;

    private void Start()
    {
        if (damageHandler == null)
            damageHandler = GetComponent<DamageHandler>();
        if (bounce == null)
            bounce = GetComponent<Bounce>();
        if (damageHandler != null)
            damageHandler.OnDamage += HandleDamage;
    }

    private void HandleDamage(GameObject damager, float amount, Vector2 direction)
    {
        if (bounce != null)
            bounce.AttemptBounce(damager.GetComponent<BounceResponder>());
    }
}
