﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyMath 
{
    public static float HorizontalDistance(Vector3 a, Transform b)
    {
        return HorizontalDistance(a, b.position);
    }

    public static float HorizontalDistance(Transform a, Vector3 b)
    {
        return HorizontalDistance(a.position, b);
    }

    public static float HorizontalDistance(Transform a, Transform b)
    {
        return HorizontalDistance(a.position, b.position);
    }

    public static float HorizontalDistance(Vector3 a, Vector3 b)
    {
        return Mathf.Abs(a.x - b.x);
    }

    public static int mod(int x, int m)
    {
        return (x % m + m) % m;
    }
}
