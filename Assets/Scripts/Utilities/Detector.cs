﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{
    public LayerMask layers;
    private List<GameObject> detectedObjects = new List<GameObject>();

    public bool Detected
    {
        get => detectedObjects.Count > 0;
    }

    private void Detect(Collider2D collider)
    {
        if ((layers.value & (1 << collider.gameObject.layer)) == 0 || detectedObjects.Contains(collider.gameObject))
            return;
        detectedObjects.Add(collider.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Detect(collider);
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        Detect(collider);
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if ((layers.value & (1 << collider.gameObject.layer)) == 0)
            return;
        detectedObjects.Remove(collider.gameObject);
    }

}
