﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LimitsComparer
{
    public ObjectLimit thisLimit;
    public ComparisonOperator comparison;
    public ObjectLimit otherLimit;

    public bool Compare(ObjectLimits first, ObjectLimits second)
    {
        float firstPoint = 0;
        float secondPoint = 0;
        switch (thisLimit)
        {
            case ObjectLimit.TopPoint:
                firstPoint = first.topPoint.position.y;
                break;
            case ObjectLimit.RightPoint:
                firstPoint = first.rightPoint.position.x;
                break;
            case ObjectLimit.BottomPoint:
                firstPoint = first.bottomPoint.position.y;
                break;
            case ObjectLimit.LeftPoint:
                firstPoint = first.leftPoint.position.x;
                break;
        }
        switch (otherLimit)
        {
            case ObjectLimit.TopPoint:
                secondPoint = second.topPoint.position.y;
                break;
            case ObjectLimit.RightPoint:
                secondPoint = second.rightPoint.position.x;
                break;
            case ObjectLimit.BottomPoint:
                secondPoint = second.bottomPoint.position.y;
                break;
            case ObjectLimit.LeftPoint:
                secondPoint = second.leftPoint.position.x;
                break;
        }
        bool valid = false;
        switch (comparison)
        {
            case ComparisonOperator.Greater:
                valid = firstPoint > secondPoint;
                break;
            case ComparisonOperator.Equal:
                valid = firstPoint == secondPoint;
                break;
            case ComparisonOperator.Less:
                valid = firstPoint < secondPoint;
                break;
            case ComparisonOperator.GreatEqual:
                valid = firstPoint >= secondPoint;
                break;
            case ComparisonOperator.LessEqual:
                valid = firstPoint <= secondPoint;
                break;
        }
        return valid;
    }
}
public enum ObjectLimit { TopPoint, RightPoint, BottomPoint, LeftPoint }
public enum ComparisonOperator { Greater, Equal, Less, GreatEqual, LessEqual }
