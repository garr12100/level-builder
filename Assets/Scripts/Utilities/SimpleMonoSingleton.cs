﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class SimpleMonoSingleton<T> : MonoBehaviour
    where T : SimpleMonoSingleton<T>
{
    private Scene currentScene;


    private static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance != null && _instance.applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                    "' already destroyed on application quit." +
                    " Won't create again - returning null.");
                return null;
            }
            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance != null && _instance != this)
            Destroy(this.gameObject);
        else
        {
            _instance = (T)this;
        }
    }

    private bool applicationIsQuitting = false;
    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public void OnDestroy()
    {
        applicationIsQuitting = true;
    }

    protected virtual void Update()
    {
        if (currentScene != SceneManager.GetActiveScene())
        {
            currentScene = SceneManager.GetActiveScene();
            OnSceneLoaded(currentScene);
            OnSceneLoaded();
        }
    }

    protected virtual void OnSceneLoaded(Scene scene)
    {

    }

    protected virtual void OnSceneLoaded()
    {

    }

}
