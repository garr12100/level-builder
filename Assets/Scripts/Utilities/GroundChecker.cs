using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Direction { up, down, right, left }


public class GroundChecker : MonoBehaviour
{
    public event System.Action OnNotGrounded;
    public event System.Action<Collider2D> OnGrounded;
    public Direction direction;
    public int numRays;
    public float spacing;
    public float rayDistance = .2f;
    public LayerMask groundLayers;
    public bool interactWithTriggers;
    public Color gizmoColor = Color.blue;
    public Color debugColor = Color.red;
    [Tooltip("Typically used to ignore self")]
    public List<Collider2D> ignoreColliders = new List<Collider2D>();

    public bool grounded;
    //{
    //    get
    //    {
    //        Vector2 dir = Vector2.zero;
    //        switch (direction)
    //        {
    //            case Direction.up:
    //                dir = Vector2.up;
    //                break;
    //            case Direction.down:
    //                dir = Vector2.down;
    //                break;
    //            case Direction.forward:
    //                dir = Vector2.right;
    //                break;
    //            case Direction.back:
    //                dir = Vector2.left;
    //                break;
    //        }
    //        int min = (int)Mathf.Ceil(-(float)numRays / 2);
    //        int max = numRays / 2;
    //        for (int i = min; i < max; i++)
    //        {
    //            Vector2 pos;
    //            if (direction == Direction.back || direction == Direction.forward)
    //                pos = new Vector2(transform.position.x, transform.position.y + (i * spacing));
    //            else
    //                pos = new Vector2(transform.position.x + (i * spacing), transform.position.y);
    //            if (Physics2D.Raycast(pos, dir, rayDistance, groundLayers))
    //            {
    //                return true;
    //            }
    //        }
    //        return false;
    //    }
    //}

    private void Update()
    {
        bool wasGrounded = grounded;
        CheckGround();
        if (!grounded && wasGrounded && OnNotGrounded != null)
            OnNotGrounded();
    }

    public bool CheckGround()
    {
        Vector2 dir = Vector2.zero;
        switch (direction)
        {
            case Direction.up:
                dir = Vector2.up;
                break;
            case Direction.down:
                dir = Vector2.down;
                break;
            case Direction.right:
                dir = Vector2.right;
                break;
            case Direction.left:
                dir = Vector2.left;
                break;
        }
        int min = (int)Mathf.Ceil(-(float)numRays / 2);
        int max = numRays / 2;
        for (int i = min; i <= max; i++)
        {
            Vector2 pos;
            if (direction == Direction.left || direction == Direction.right)
                pos = new Vector2(transform.position.x, transform.position.y + (i * spacing));
            else
                pos = new Vector2(transform.position.x + (i * spacing), transform.position.y);
            Debug.DrawRay(pos, dir * rayDistance, debugColor);
            RaycastHit2D[] hits;
            hits = Physics2D.RaycastAll(pos, dir, rayDistance, groundLayers);
            if (hits.Length != 0)
            {
                bool wasGrounded = grounded;
                Collider2D hitCollider = CheckIfGrounded(hits);
                if (grounded = hitCollider != null)
                {
                    if (!wasGrounded && OnGrounded != null)
                        OnGrounded(hitCollider);
                    return true;
                }
            }
        }
        grounded = false;
        return false;
    }

    Collider2D CheckIfGrounded(RaycastHit2D[] hits)
    {
        foreach (RaycastHit2D hit in hits)
        {
            if (ignoreColliders.Contains(hit.collider))
                continue;
            if (interactWithTriggers || !hit.collider.isTrigger)
                return hit.collider;
        }
        return null;
    }

    private void OnDrawGizmos()
    {
        Vector2 dir = Vector2.zero;
        switch (direction)
        {
            case Direction.up:
                dir = Vector2.up;
                break;
            case Direction.down:
                dir = Vector2.down;
                break;
            case Direction.right:
                dir = Vector2.right;
                break;
            case Direction.left:
                dir = Vector2.left;
                break;
        }
        int min = (int)Mathf.Ceil(-(float)numRays / 2);
        int max = numRays / 2;
        for (int i = min; i <= max; i++)
        {
            Vector2 pos;
            if (direction == Direction.left || direction == Direction.right)
                pos = new Vector2(transform.position.x, transform.position.y + (i * spacing));
            else
                pos = new Vector2(transform.position.x + (i * spacing), transform.position.y);
            Gizmos.color = gizmoColor;
            Gizmos.DrawRay(pos, dir * rayDistance);
        }
    }
    //   Collider2D col;
    //// Use this for initialization
    //void Start ()
    //   {
    //       col = GetComponent<Collider2D>();
    //}

    //   // Update is called once per frame
    //   void OnTriggerStay2D(Collider2D otherCol)
    //   {
    //       if (otherCol.CompareTag(UtilityStrings.Tags.Level))
    //       {
    //           grounded = true;
    //       }
    //   }

    //   void OnTriggerExit2D(Collider2D otherCol)
    //   {
    //       if (otherCol.CompareTag(UtilityStrings.Tags.Level))
    //       {
    //           if (grounded && OnNotGrounded != null)
    //               OnNotGrounded();
    //           grounded = false;

    //       }
    //   }
}
