﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenericDelayedAction : MonoBehaviour
{
    public float delay;
    public UnityEvent preAction;
    public UnityEvent postAction;

    public void StartDelay()
    {
        StartCoroutine(DelayAction());
    }

    private IEnumerator DelayAction()
    {
        preAction.Invoke();
        yield return new WaitForSeconds(delay);
        postAction.Invoke();
    }
}
