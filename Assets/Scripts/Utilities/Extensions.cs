﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static void AddExplosionForce(this Rigidbody2D rb, float explosionForce, Vector2 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode2D mode = ForceMode2D.Force)
    {
        var explosionDir = rb.position - explosionPosition;
        var explosionDistance = (explosionDir.magnitude / explosionRadius);

        // Normalize without computing magnitude again
        if (upwardsModifier == 0)
        {
            explosionDir /= explosionDistance;
        }
        else
        {
            // If you pass a non-zero value for the upwardsModifier parameter, the direction
            // will be modified by subtracting that value from the Y component of the centre point.
            explosionDir.y += upwardsModifier;
            explosionDir.Normalize();
        }
        Debug.Log("Force: " + Mathf.Lerp(0, explosionForce, (1 - explosionDistance)) + ", Dir: " + explosionDir);
        rb.AddForce(Mathf.Lerp(0, explosionForce, (1 - explosionDistance)) * explosionDir, mode);
    }

    public static float Average(this List<float> floats)
    {
        if (floats.Count == 0)
            return 0;
        float avg = 0;
        foreach (var f in floats)
            avg += f;
        return avg / floats.Count;
    }

    public static Vector3 Average(this List<Vector3> vectors)
    {
        if (vectors.Count == 0)
            return Vector3.zero;
        Vector3 avg = Vector3.zero;
        foreach (var v in vectors)
            avg += v;
        return avg / vectors.Count;
    }
}
