﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLimits : MonoBehaviour
{
    public Transform bottomPoint;
    public Transform leftPoint;
    public Transform topPoint;
    public Transform rightPoint;
}
