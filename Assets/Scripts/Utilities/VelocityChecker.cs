﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VelocityChecker
{
    public XY xy;
    public ComparisonOperator comparison;
    public float threshold;
    public bool falseIfNull = false;
    public bool ignore;
    public Rigidbody2D _rbody;

    public bool CheckVelocity()
    {
        return CheckVelocity(_rbody);
    }

    public bool CheckVelocity(Rigidbody2D rbody)
    {
        if (ignore)
            return true;
        if(rbody == null)
            return !falseIfNull;
        float v = 0;
        switch (xy)
        {
            case XY.x:
                v = rbody.velocity.x;
                break;
            case XY.y:
                v = rbody.velocity.y;
                break;
        }
        switch (comparison)
        {
            case ComparisonOperator.Greater:
                return v > threshold;
            case ComparisonOperator.Equal:
                return v == threshold;
            case ComparisonOperator.Less:
                return v < threshold;
            case ComparisonOperator.GreatEqual:
                return v >= threshold;
            case ComparisonOperator.LessEqual:
                return v <= threshold;
        }
        return false;
    }
}

public enum XY { x, y }
