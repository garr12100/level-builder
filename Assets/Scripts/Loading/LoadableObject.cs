﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadableObject : MonoBehaviour
{
    public float loadInDistance = 20f;
    public float unloadDistance = 20f;

    public bool isLoadable = true;

    private bool active = false;
    public bool Active
    {
        get => active;
    }

    private Vector3 startingPosition;
    public Vector3 StartingPostion
    {
        get => startingPosition;
    }

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        Unload();
        ObjectLoadingManager.Instance.Add(this);
    }

    public void Load()
    {
        if (!isLoadable)
            return;
        active = true;
        gameObject.SetActive(true);
        transform.position = startingPosition;
    }

    public void Unload()
    {
        active = false;
        gameObject.SetActive(false);
        transform.position = startingPosition;
    }

    private void OnDestroy()
    {
        if (ObjectLoadingManager.Instance)
            ObjectLoadingManager.Instance.Remove(this);
    }

}
