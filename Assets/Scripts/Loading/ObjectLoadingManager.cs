﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLoadingManager : SimpleMonoSingleton<ObjectLoadingManager>
{
    public List<LoadableObject> registeredObjects;
    public Transform playerTransform;

    public void Add(LoadableObject newObject)
    {
        if (registeredObjects.Contains(newObject))
            return;
        registeredObjects.Add(newObject);
    }

    public void Remove(LoadableObject oldObject)
    {
        if (!registeredObjects.Contains(oldObject))
            return;
        registeredObjects.Remove(oldObject);
    }

    public void HandleRespawn()
    {
        foreach (var obj in registeredObjects)
        {
            obj.isLoadable = true;
            obj.Unload();
            obj.Load();
        }
    }

    protected void FixedUpdate()
    {
        if (playerTransform == null)
            return;
        foreach (var obj in registeredObjects)
        {
            if (obj.Active)
            {
                if (MyMath.HorizontalDistance(playerTransform, obj.transform) > obj.unloadDistance && MyMath.HorizontalDistance(playerTransform, obj.StartingPostion) > obj.unloadDistance)
                    obj.Unload();
            }
            else if (obj.isLoadable)
            {
                if (MyMath.HorizontalDistance(playerTransform, obj.transform) <= obj.loadInDistance)
                    obj.Load();
            }
        }
    }
}
