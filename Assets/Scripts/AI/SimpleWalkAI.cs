﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleWalkAI : MonoBehaviour
{
    public bool initiallyFaceRight;
    public bool edgeDetection;
    public Movement movement;
    public GroundChecker edgeDetecter;

    float moveX;
    void OnEnable()
    {
        if (movement == null)
            movement = GetComponent<Movement>();

        if (initiallyFaceRight)
        {
            moveX = 1;
            if (!movement.FacingRight)
                movement.Flip();
        }
        else
        {
            moveX = -1;
            if (movement.FacingRight)
                movement.Flip();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (movement.wallChecker.CheckGround())
            moveX *= -1;
        else if (edgeDetection && edgeDetecter != null && !edgeDetecter.CheckGround() && movement.groundChecker.CheckGround())
        {
            moveX *= -1;
        }
        movement.MoveX = moveX;

    }
}
