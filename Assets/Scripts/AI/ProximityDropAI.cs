﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityDropAI : MonoBehaviour
{
    public enum State { Ready, Waiting, Attacking, Returning };
    public Direction direction = Direction.down;
    public bool attackOnReturn;
    public float waitTime = 1f;
    public float returnSpeedMultiplier = .1f;
    public State state = State.Ready;
    public Movement movement;
    public Detector detectorUp;
    public Detector detectorDown;
    public Detector detectorRight;
    public Detector detectorLeft;
    public GroundChecker checkerUp;
    public GroundChecker checkerDown;
    public GroundChecker checkerRight;
    public GroundChecker checkerLeft;
    private Vector3 initialPosition;
    private float defaultAccel;

    private void OnEnable()
    {
        if (movement == null)
            movement = GetComponent<Movement>();
        initialPosition = transform.position;
        defaultAccel = movement.inAirxAccel;
        state = State.Ready;
    }

    private void FixedUpdate()
    {
        Detector detector = null;
        GroundChecker checker = null;
        switch (direction)
        {
            case Direction.up:
                detector = detectorUp;
                checker = checkerUp;
                movement.Rbody.constraints = RigidbodyConstraints2D.FreezePositionX;
                break;
            case Direction.down:
                detector = detectorDown;
                checker = checkerDown;
                movement.Rbody.constraints = RigidbodyConstraints2D.FreezePositionX;
                break;
            case Direction.right:
                detector = detectorRight;
                checker = checkerRight;
                movement.Rbody.constraints = RigidbodyConstraints2D.FreezePositionY;
                break;
            case Direction.left:
                detector = detectorLeft;
                checker = checkerLeft;
                movement.Rbody.constraints = RigidbodyConstraints2D.FreezePositionY;
                break;
        }
        if (movement == null || detector == null || checker == null)
            return;
        switch (state)
        {
            case State.Ready:
                transform.position = initialPosition;
                movement.inAirxAccel = defaultAccel;
                movement.xAccel = defaultAccel;
                SetMovement(0);
                if (detector.Detected && !checker.grounded)
                {
                    state = State.Attacking;
                }
                break;
            case State.Waiting:
                break;
            case State.Attacking:
                SetMovement(1);
                if (checker.grounded)
                    StartCoroutine(Wait());
                break;
            case State.Returning:
                if (attackOnReturn && detector.Detected && !checker.grounded)
                {
                    state = State.Attacking;
                    break;
                }
                if (AtInitialPosition())
                {
                    movement.inAirxAccel = 1;
                    movement.xAccel = 1;
                    SetMovement(0);
                    state = State.Ready;
                }
                else
                    SetMovement(-returnSpeedMultiplier);
                break;
        }
    }

    bool AtInitialPosition()
    {
        switch (direction)
        {
            case Direction.up:
                return transform.position.y <= initialPosition.y;
            case Direction.down:
                return transform.position.y >= initialPosition.y;
            case Direction.right:
                return transform.position.x <= initialPosition.x;
            case Direction.left:
                return transform.position.x >= initialPosition.x;
        }
        return false;
    }

    void SetMovement(float val)
    {
        switch (direction)
        {
            case Direction.up:
                movement.MoveY = val;
                break;
            case Direction.down:
                movement.MoveY = -val;
                break;
            case Direction.right:
                movement.MoveX = val;
                break;
            case Direction.left:
                movement.MoveX = -val;
                break;
        }
    }

    IEnumerator Wait()
    {
        state = State.Waiting;
        yield return new WaitForSeconds(waitTime);
        state = State.Returning;
    }
}
