﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour
{

    public float jumpHeight = 5f;
    public int numJumps = 1;
    public float variableJumpHoldFrames = 10f;
    public GroundChecker groundChecker;

    Rigidbody2D rbody;
    public int numJumpsRemaining;

    float gravityScale;

    private bool jumpThisFrame;
    private bool attemptingToJump;

    public bool JumpThisFrame
    {
        get => jumpThisFrame;
        set => jumpThisFrame = value;
    }

    public bool AttemptingToJump
    {
        get => attemptingToJump;
        set => attemptingToJump = value;
    }

    // Use this for initialization
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        gravityScale = rbody.gravityScale;
        groundChecker.OnNotGrounded += OnNotGrounded;
    }

    void FixedUpdate()
    {
        //Reset jumps if on ground
        if (groundChecker.grounded)
        {
            RestoreJumps();
        }
        float jumpH = jumpHeight;
        //Execute jump
        if (jumpThisFrame)
        {
            AttemptJump(jumpH);
        }
        jumpThisFrame = false;

        //Set speed to lerp to target
        rbody.velocity = new Vector2(rbody.velocity.x, rbody.velocity.y);

    }

    public void RestoreJumps()
    {
        numJumpsRemaining = numJumps;
    }

    void AttemptJump(float height)
    {
        if (numJumpsRemaining > 0)
        {
            if (!groundChecker.grounded)
                numJumpsRemaining--;
            PerformJump(height);
        }
    }

    public void PerformJump(float height)
    {
        rbody.gravityScale = 0;
        rbody.velocity = new Vector2(rbody.velocity.x, height);
        StartCoroutine(JumpContinue());
    }

    public void OnNotGrounded()
    {
        numJumpsRemaining--;
    }

    IEnumerator JumpContinue()
    {
        int t = 0;
        while (true)
        {
            if (t > variableJumpHoldFrames)
            {
                rbody.gravityScale = gravityScale;
                yield break;
            }
            t++;
            yield return new WaitForFixedUpdate();
            if (!attemptingToJump)
            {
                rbody.gravityScale = gravityScale;
                yield break;
            }
        }
    }
}
