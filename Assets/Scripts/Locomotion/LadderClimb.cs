﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LadderClimb : MonoBehaviour
{

    public float climbSpeed = 10f;
    public float ladderJumpPauseClimbTime = .25f;
    public bool canClimbLadders;
    public GroundChecker groundChecker;

    Rigidbody2D rbody;
    Movement movement;
    Jump jump;
    bool touchingLadder;
    bool climbingLadder;
    List<GameObject> ladders = new List<GameObject>();

    float gravityScale;

    private GameObject NearestLadder
    {
        get
        {
            int nearestIndex = -1;
            float nearestDistance = Mathf.Infinity;
            for (int i = 0; i < ladders.Count; i++)
            {
                float d = Vector3.Distance(ladders[i].transform.position, rbody.transform.position);
                if (d < nearestDistance)
                {
                    nearestDistance = d;
                    nearestIndex = i;
                }
            }
            if (nearestIndex < 0)
                return null;
            return ladders[nearestIndex];
        }
    }
  

    // Use this for initialization
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        movement = GetComponent<Movement>();
        jump = GetComponent<Jump>();
        gravityScale = rbody.gravityScale;
        canClimbLadders = true;
    }

    void FixedUpdate()
    {
        if (jump != null && jump.JumpThisFrame && climbingLadder)
            StartCoroutine(StopLadderClimbingForTime(ladderJumpPauseClimbTime));
        //Check if ladder:
        if (touchingLadder && (Mathf.Abs(movement.MoveY) > 0f || climbingLadder) && canClimbLadders)
        {
            if (!groundChecker.grounded || movement.MoveY > 0f)
            {
                climbingLadder = true;
                rbody.velocity = new Vector2(0, climbSpeed * movement.MoveY);
                rbody.gravityScale = 0;
                movement.canMoveX = false;
                movement.ResetTargetX();
                transform.position = new Vector3(NearestLadder.transform.position.x + .5f, transform.position.y, transform.position.z);
                if (jump != null)
                    jump.RestoreJumps();
            }
            else
                HandleExitLadder();
        }
        //else if (!canClimbLadders)
        //{
        //    HandleExitLadder();
        //}

    }

    IEnumerator StopLadderClimbingForTime(float t)
    {
        canClimbLadders = false;
        HandleExitLadder(false);
        yield return new WaitForSeconds(t);
        canClimbLadders = true;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag(UtilityStrings.Tags.Ladder))
        {
            ladders.Add(col.gameObject);
            touchingLadder = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag(UtilityStrings.Tags.Ladder))
        {
            ladders.Remove(col.gameObject);
            if (ladders.Count == 0)
            {
                touchingLadder = false;
                if (climbingLadder)
                    HandleExitLadder();
            }
        }
    }

    void HandleExitLadder(bool resetGravity = true)
    {
        climbingLadder = false;
        if(resetGravity)
            rbody.gravityScale = gravityScale;
        movement.canMoveX = true;
    }
}
