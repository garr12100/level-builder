﻿using UnityEngine;
using System.Collections;

public class WallJump : MonoBehaviour
{
    public float wallSlideSpeed = 1f;
    public float wallJumpHorizontalSpeed;
    public float wallJumpPauseMoveTime;
    public float wallJumpHeight = 5f;
    public float wallGrabTimer = 0f;
    public int numJumpsFromWall = 1;
    public GroundChecker groundChecker;
    public GroundChecker wallChecker;

    Movement movement;
    Jump jump;
    Rigidbody2D rbody;
    bool wallSliding = false;

    float gravityScale;


    // Use this for initialization
    void Start()
    {
        movement = GetComponent<Movement>();
        jump = GetComponent<Jump>();
        rbody = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        wallSliding = false;
        //Look for being against wall
        if (!groundChecker.grounded && wallChecker != null && wallChecker.grounded)
        {
            //Check if user trying to move against wall
            if ((movement.FacingRight && movement.MoveX > 0) || (!movement.FacingRight && movement.MoveX < 0))
            {
                if (rbody.velocity.y < 0)
                {
                    wallSliding = true;
                    rbody.velocity = new Vector2(rbody.velocity.x, -wallSlideSpeed);
                }
                int dir = movement.FacingRight ? -1 : 1;
                if (jump.JumpThisFrame)
                {
                    PerformWallJump(dir);
                }
            }
        }
    }

    void PerformWallJump(int dir)
    {
        jump.PerformJump(wallJumpHeight);
        rbody.velocity = new Vector2(dir * wallJumpHorizontalSpeed, rbody.velocity.y);
        movement.ModifyTargetXVelocity(dir * wallJumpHorizontalSpeed);
        movement.PauseMovement(wallJumpPauseMoveTime);
        movement.Flip();
        jump.RestoreJumps();

    }



}
