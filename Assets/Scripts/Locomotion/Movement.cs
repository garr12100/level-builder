﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{

    public float speedWalk = 10f;
    public float speedRun = 15;
    public float groundRadius = .1f;
    public float xAccel = .25f;
    public float inAirxAccel = .05f;
    public float maxFallSpeed = -20f;
    public GroundChecker groundChecker;
    public GroundChecker wallChecker;
    public bool canMoveX;
    public bool canMoveY;
    public bool running;
    public bool disallowFlip;

    Rigidbody2D rbody;
    SpriteRenderer spriteRenderer;
    bool facingRight = true;

    float targetXVel;
    float targetYVel;
    float xModifications;
    float gravityScale;
    float moveX, moveY;

    private bool jumpThisFrame;


    public Rigidbody2D Rbody
    {
        get => rbody;
    }

    public float MoveX
    {
        get => moveX;
        set => moveX = value;
    }

    public float MoveY
    {
        get => moveY;
        set => moveY = value;
    }

    public bool FacingRight
    {
        get => facingRight;
    }

    public bool AtMaxSpeed
    {
        get
        {
            float maxSpeed = speedRun - .01f;
            return Mathf.Abs(rbody.velocity.x) >= maxSpeed;
        }
    }

    // Use this for initialization
    void Start()
    {
        rbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        gravityScale = rbody.gravityScale;
        canMoveX = true;
    }

    void FixedUpdate()
    {
        if (canMoveX)
        {
            targetXVel = moveX * (running ? speedRun : speedWalk);
            //Check for flipping
            if (moveX > 0 && !facingRight) Flip();
            else if (moveX < 0 && facingRight) Flip();
        }
        if (canMoveY)
        {
            targetYVel = moveY * (running ? speedRun : speedWalk);
        }
        targetXVel += xModifications;
        xModifications = 0;
        //Set speed to lerp to target
        float accel = (groundChecker == null || groundChecker.grounded) ? xAccel : inAirxAccel;
        float yVal = canMoveY ? Mathf.Lerp(rbody.velocity.y, targetYVel, accel) : Mathf.Clamp(rbody.velocity.y, maxFallSpeed, Mathf.Infinity);
        rbody.velocity = new Vector2(Mathf.Lerp(rbody.velocity.x, targetXVel, accel), yVal);
    }

    public void Flip()
    {
        if (disallowFlip)
            return;
        facingRight = !facingRight;
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        if (facingRight)
            wallChecker.direction = Direction.right;
        else
            wallChecker.direction = Direction.left;
    }

    IEnumerator StopMovementForTime(float t)
    {
        canMoveX = false;
        yield return new WaitForSeconds(t);
        canMoveX = true;
    }

    public void PauseMovement(float t)
    {
        targetXVel = 0;
        StartCoroutine(StopMovementForTime(t));
    }

    public void ModifyTargetXVelocity(float amount)
    {
        xModifications += amount;
    }

    public void ResetTargetX()
    {
        targetXVel = 0;
    }

    public void OnDisable()
    {
        ResetTargetX();
        moveX = 0;
        MoveY = 0;
        if (rbody)
            rbody.velocity = Vector2.zero;
    }

    public void FreezePosition()
    {
        if (rbody)
        {
            rbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    public void UnfreezePosition()
    {
        if (rbody)
            rbody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }
}
