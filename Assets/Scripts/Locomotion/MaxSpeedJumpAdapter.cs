﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxSpeedJumpAdapter : MonoBehaviour
{
    public float maxSpeedJumpHeight = 15f;
    public Movement movement;
    public Jump jump;

    private float defaultJumpHeight;

    private void Start()
    {
        if (movement == null)
            movement = GetComponent<Movement>();
        if (jump == null)
            jump = GetComponent<Jump>();
        if (jump != null)
            defaultJumpHeight = jump.jumpHeight;
    }

    private void FixedUpdate()
    {
        if (movement == null || jump == null)
            return;
        if (movement.AtMaxSpeed)
        {
            jump.jumpHeight = maxSpeedJumpHeight;
        }
        else
            jump.jumpHeight = defaultJumpHeight;
    }

}
