using UnityEngine;
using System.Collections.Generic;

public class Semisolid_Dropthrough : Semisolid
{
    protected override bool Solid(Collider2D collider)
    {
        var movement = collider.GetComponent<Movement>();
        if (movement == null)
            return base.Solid(collider);
        return !(movement.MoveY < 0) && base.Solid(collider);
    }

}
