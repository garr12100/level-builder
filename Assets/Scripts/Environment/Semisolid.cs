using UnityEngine;
using System.Collections.Generic;

public class Semisolid : MonoBehaviour
{
    public Collider2D floorCollider;
    public ObjectLimits objectLimits;
    public LimitsComparer limitsComparer;
    public LayerMask layerMask;
    public bool solidIfLimitsNull;
    public bool debug;

    private void Start()
    {
        if (objectLimits == null)
            objectLimits = GetComponent<ObjectLimits>();
    }

    protected void OnTriggerEnter2D(Collider2D collider)
    {
        HandleCollision(collider);
    }

    protected virtual void OnTriggerStay2D(Collider2D collider)
    {
        HandleCollision(collider);
    }

    void HandleCollision(Collider2D collider)
    {
        if ((layerMask.value & (1 << collider.gameObject.layer)) == 0)
            return;
        if (debug && !Solid(collider))
            Debug.Log(transform.parent.name + " is not solid with " + collider.name);
        Physics2D.IgnoreCollision(collider, floorCollider.GetComponent<Collider2D>(), !Solid(collider));
    }

    protected virtual bool Solid(Collider2D collider)
    {
        var otherLimits = collider.GetComponent<ObjectLimits>();
        if (objectLimits == null || otherLimits == null)
            return solidIfLimitsNull;
        return limitsComparer.Compare(objectLimits, otherLimits);
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        Physics2D.IgnoreCollision(collision, floorCollider.GetComponent<Collider2D>(), false);
        if (debug)
            Debug.Log(transform.parent.name + " IS SOLID AGAIN WITH " + collision.name);
    }

}
