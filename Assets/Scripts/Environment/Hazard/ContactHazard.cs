﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactHazard : MonoBehaviour
{
    public float damage = 1f;
    public bool directional = true;
    public LayerMask layers;
    public List<string> affectedTags;
    public Transform origin;

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        CheckForContact(otherCollider);
    }

    private void OnTriggerStay2D(Collider2D otherCollider)
    {
        CheckForContact(otherCollider);
    }

    protected virtual void CheckForContact(Collider2D otherCollider)
    {
        if ((layers.value & (1 << otherCollider.gameObject.layer)) == 0)
            return;
        var damageHandler = otherCollider.GetComponent<DamageHandler>();
        if (damageHandler != null)
        {
            if (directional)
            {
                Vector3 pos = origin == null ? transform.position : origin.position;
                Vector2 dir = otherCollider.transform.position - pos;
                damageHandler.TakeDamage(this.gameObject, damage, dir);
            }
            else
                damageHandler.TakeDamage(this.gameObject, damage);
        }
    }

}
