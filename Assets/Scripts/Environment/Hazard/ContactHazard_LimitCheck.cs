﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactHazard_LimitCheck : ContactHazard
{

    public ObjectLimits objectLimits;
    public bool ignoreLimits;
    public LimitsComparer limitsComparer;
    public VelocityChecker thisVelocityChecker, otherVelocityChecker;
    public bool debug;

    protected override void CheckForContact(Collider2D otherCollider)
    {
        if (debug)
            Debug.Log("Entered Check");
        if ((layers.value & (1 << otherCollider.gameObject.layer)) == 0)
            return;
        if (debug)
            Debug.Log("Layer Good");
        var otherLimits = otherCollider.GetComponentInParent<ObjectLimits>();
        if (!ignoreLimits && otherLimits != null && objectLimits != null)
        {
            if (!limitsComparer.Compare(objectLimits, otherLimits))
                return;
        }
        if (debug)
            Debug.Log("Limits Good");
        if (!thisVelocityChecker.CheckVelocity())
            return;
        if (debug)
            Debug.Log("this V Check Good");
        if (!otherVelocityChecker.CheckVelocity())
            return;
        if (debug)
            Debug.Log("other V Check Good");
        base.CheckForContact(otherCollider);
    }
}

